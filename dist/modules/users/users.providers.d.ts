import { User } from './entities/user.entity';
export declare const usersProviders: {
    provide: string;
    useValue: typeof User;
}[];
