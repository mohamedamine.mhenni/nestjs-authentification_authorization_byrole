import { Model } from 'sequelize-typescript';
import { Role } from 'src/core/database/constants';
export declare class User extends Model {
    name: string;
    email: string;
    password: string;
    gender: string;
    roles: Role[];
}
