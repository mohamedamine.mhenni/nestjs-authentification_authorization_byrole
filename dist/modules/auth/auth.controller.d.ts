import { CreateUserDto } from '../users/dto/create-user.dto';
import { AuthService } from './auth.service';
import { CreateAuthDto } from './dto/create-auth.dto';
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    login(req: any): Promise<{
        user: CreateAuthDto;
        token: string;
    }>;
    signUp(user: CreateUserDto): Promise<{
        user: any;
        token: string;
    }>;
    verify(req: any): Promise<{
        status: number;
        data: {
            user: any;
        };
        message: string;
        error?: undefined;
    } | {
        status: number;
        error: any;
        message: string;
        data?: undefined;
    }>;
}
