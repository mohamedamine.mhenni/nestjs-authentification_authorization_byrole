"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RolesValidator = void 0;
const class_validator_1 = require("class-validator");
let RolesValidator = class RolesValidator {
    validate(roles, args) {
        return (roles.every((elem) => args.constraints.includes(elem)) &&
            args.constraints.some((role) => roles === null || roles === void 0 ? void 0 : roles.includes(role)));
    }
    defaultMessage(args) {
        return 'Roles should contains admin or user!';
    }
};
RolesValidator = __decorate([
    (0, class_validator_1.ValidatorConstraint)({ name: 'customText', async: false })
], RolesValidator);
exports.RolesValidator = RolesValidator;
//# sourceMappingURL=RolesValidator.js.map