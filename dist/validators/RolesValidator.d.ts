import { ValidatorConstraintInterface, ValidationArguments } from 'class-validator';
export declare class RolesValidator implements ValidatorConstraintInterface {
    validate(roles: string[], args: ValidationArguments): boolean;
    defaultMessage(args: ValidationArguments): string;
}
