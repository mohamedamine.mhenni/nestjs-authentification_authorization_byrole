export declare const SEQUELIZE = "SEQUELIZE";
export declare const DEVELOPMENT = "development";
export declare const TEST = "test";
export declare const PRODUCTION = "production";
export declare const USER_REPOSITORY = "USER_REPOSITORY";
export declare const POST_REPOSITORY = "POST_REPOSITORY";
export declare const PROJECT_CONTRIBUTOR_REPOSITORY = "PROJECT_CONTRIBUTOR_REPOSITORY";
export declare const PROJECT_REPOSITORY = "PROJECT_REPOSITORY";
export declare enum Role {
    User = "user",
    Admin = "admin"
}
