import { Strategy } from 'passport-jwt';
import { ConfigService } from '@nestjs/config';
import { UsersService } from 'src/modules/users/users.service';
declare const JwtStrategy_base: new (...args: any[]) => Strategy;
export declare class JwtStrategy extends JwtStrategy_base {
    private configService;
    private readonly userService;
    constructor(configService: ConfigService, userService: UsersService);
    validate(payload: any): Promise<any>;
}
export {};
