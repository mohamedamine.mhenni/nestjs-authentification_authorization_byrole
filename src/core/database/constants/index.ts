export const SEQUELIZE = 'SEQUELIZE';
export const DEVELOPMENT = 'development';
export const TEST = 'test';
export const PRODUCTION = 'production';
export const USER_REPOSITORY = 'USER_REPOSITORY';
export const POST_REPOSITORY = 'POST_REPOSITORY';
export const PROJECT_CONTRIBUTOR_REPOSITORY = 'PROJECT_CONTRIBUTOR_REPOSITORY';
export const PROJECT_REPOSITORY = 'PROJECT_REPOSITORY';
export enum Role {
  User = 'user',
  Admin = 'admin',
}
