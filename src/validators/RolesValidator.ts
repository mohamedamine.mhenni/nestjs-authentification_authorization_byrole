import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ name: 'customText', async: false })
export class RolesValidator implements ValidatorConstraintInterface {
  validate(roles: string[], args: ValidationArguments) {
    return (
      roles.every((elem) => args.constraints.includes(elem)) &&
      args.constraints.some((role) => roles?.includes(role))
    );
  }

  defaultMessage(args: ValidationArguments) {
    // here you can provide default error message if validation failed
    return 'Roles should contains admin or user!';
  }
}
